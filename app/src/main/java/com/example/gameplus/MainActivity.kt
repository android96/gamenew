package com.example.gameplus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val page1 = findViewById<Button>(R.id.btnMainPlus)
        page1.setOnClickListener {
            val intent = Intent(MainActivity@this, Plus::class.java)
            startActivity(intent)
        }
        val page2 = findViewById<Button>(R.id.btnMainDelete)
        page2.setOnClickListener {
            val intent = Intent(MainActivity@this, Delete::class.java)
            startActivity(intent)
        }
        val page3 = findViewById<Button>(R.id.btnMainMuti)
        page3.setOnClickListener {
            val intent = Intent(MainActivity@this, Muti::class.java)
            startActivity(intent)
        }

    }


}
