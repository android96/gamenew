package com.example.gameplus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_delete.*
import kotlinx.android.synthetic.main.activity_muti.*
import kotlinx.android.synthetic.main.activity_plus.*
import kotlin.random.Random

class Muti : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_muti)

        val homeMuti = findViewById<ImageView>(R.id.btnHome3)
        homeMuti.setOnClickListener {
            val intent = Intent(Muti@this, MainActivity::class.java)
            startActivity(intent)
        }

        playMuti()
        btnNextMuti.setOnClickListener{
            resultMuti.setText("Please Select an Answer")
            playMuti()
        }

    }

    private fun playMuti() {
        val random1: Int = Random.nextInt(10) + 1
        textViewMuti.setText(Integer.toString(random1))

        val random2: Int = Random.nextInt(10) + 1
        textViewMuti2.setText(Integer.toString(random2))

        val sum = random1 * random2

        val position: Int = Random.nextInt(3) + 1

        if (position == 1) {
            btnMuti1.setText(Integer.toString(sum))
            btnMuti2.setText(Integer.toString(sum - 1))
            btnMuti3.setText(Integer.toString(sum + 2))
        } else if (position == 2) {
            btnMuti1.setText(Integer.toString(sum))
            btnMuti2.setText(Integer.toString(sum - 2))
            btnMuti3.setText(Integer.toString(sum + 1))
        } else {
            btnMuti3.setText(Integer.toString(sum))
            btnMuti2.setText(Integer.toString(sum - 2))
            btnMuti1.setText(Integer.toString(sum + 3))
        }
        btnMuti1.setOnClickListener {
            if (btnMuti1.text.toString().toInt() == sum) {
                resultMuti.setText("Correct")
                correctMuti.text = (correctMuti.text.toString().toInt() + 1).toString()

            } else {
                resultMuti.setText("Wrong")
                wrongMuti.text = (wrongMuti.text.toString().toInt() + 1).toString()
            }
        }
        btnMuti2.setOnClickListener {
            if (btnMuti2.text.toString().toInt() == sum) {
                resultMuti.setText("Correct")
                correctMuti.text = (correctMuti.text.toString().toInt() + 1).toString()

            } else {
                resultMuti.setText("Wrong")
                wrongMuti.text = (wrongMuti.text.toString().toInt() + 1).toString()
            }
        }
        btnMuti3.setOnClickListener {
            if (btnMuti3.text.toString().toInt() == sum) {
                resultMuti.setText("Correct")
                correctMuti.text = (correctMuti.text.toString().toInt() + 1).toString()

            } else {
                resultMuti.setText("Wrong")
                wrongMuti.text = (wrongMuti.text.toString().toInt() + 1).toString()
            }
        }
    }
}