package com.example.gameplus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_delete.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_plus.*
import kotlin.random.Random

class Delete : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete)

        val homedelete = findViewById<ImageView>(R.id.btnHome2)
        homedelete.setOnClickListener {
            val intent = Intent(Delete@this, MainActivity::class.java)
            startActivity(intent)
        }

        PlayDelete()
        btnNextDelete.setOnClickListener{
            resultDelete.setText("Please Select an Answer")
            PlayDelete()
        }
    }

    private fun PlayDelete() {
        val random1: Int = Random.nextInt(10) + 1
        textViewDelete.setText(Integer.toString(random1))

        val random2: Int = Random.nextInt(10) + 1
        textViewDelete2.setText(Integer.toString(random2))

        val sum = random1 - random2

        val position: Int = Random.nextInt(3) + 1

        if (position == 1) {
            btnDelete1.setText(Integer.toString(sum))
            btnDelete2.setText(Integer.toString(sum - 1))
            btnDelete3.setText(Integer.toString(sum + 2))
        } else if (position == 2) {
            btnDelete2.setText(Integer.toString(sum))
            btnDelete1.setText(Integer.toString(sum - 2))
            btnDelete3.setText(Integer.toString(sum + 1))
        } else {
            btnDelete3.setText(Integer.toString(sum))
            btnDelete2.setText(Integer.toString(sum - 2))
            btnDelete1.setText(Integer.toString(sum + 3))
        }
        btnDelete1.setOnClickListener {
            if (btnDelete1.text.toString().toInt() == sum) {
                resultDelete.setText("Correct")
                correctDelete.text = (correctDelete.text.toString().toInt() + 1).toString()

            } else {
                resultDelete.setText("Wrong")
                wrongDelete.text = (wrongDelete.text.toString().toInt() + 1).toString()
            }
        }
        btnDelete2.setOnClickListener {
            if (btnDelete2.text.toString().toInt() == sum) {
                resultDelete.setText("Correct")
                correctDelete.text = (correctDelete.text.toString().toInt() + 1).toString()

            } else {
                resultDelete.setText("Wrong")
                wrongDelete.text = (wrongDelete.text.toString().toInt() + 1).toString()
            }
        }
        btnDelete3.setOnClickListener {
            if (btnDelete3.text.toString().toInt() == sum) {
                resultDelete.setText("Correct")
                correctDelete.text = (correctDelete.text.toString().toInt() + 1).toString()

            } else {
                resultDelete.setText("Wrong")
                wrongDelete.text = (wrongDelete.text.toString().toInt() + 1).toString()
            }
        }
    }
}