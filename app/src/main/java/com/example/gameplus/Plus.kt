package com.example.gameplus

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_plus.*
import kotlin.random.Random

class Plus : AppCompatActivity() {
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus)

        val home = findViewById<ImageView>(R.id.btnHome)
        home.setOnClickListener {
            val intent = Intent(MainActivity@this, MainActivity::class.java)
            startActivity(intent)
        }

        Play()
        btnNextPlus.setOnClickListener{
            resultPlus.setText("Please Select an Answer")
            Play()
        }



    }

    fun Play() {
        val random1: Int = Random.nextInt(10) + 1
        textViewPlus1.setText(Integer.toString(random1))

        val random2: Int = Random.nextInt(10) + 1
        textViewPlus2.setText(Integer.toString(random2))

        val sum = random1 + random2

        val position: Int = Random.nextInt(3) + 1

        if (position == 1) {
            btnPlus1.setText(Integer.toString(sum))
            btnPlus2.setText(Integer.toString(sum - 1))
            btnPlus3.setText(Integer.toString(sum + 2))
        } else if (position == 2) {
            btnPlus2.setText(Integer.toString(sum))
            btnPlus1.setText(Integer.toString(sum - 2))
            btnPlus3.setText(Integer.toString(sum + 1))
        } else {
            btnPlus3.setText(Integer.toString(sum))
            btnPlus2.setText(Integer.toString(sum - 2))
            btnPlus1.setText(Integer.toString(sum + 3))
        }

        btnPlus1.setOnClickListener {
            if (btnPlus1.text.toString().toInt() == sum) {
                resultPlus.setText("Correct")
                correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

            } else {
                resultPlus.setText("Wrong")
                wrongPlus.text = (wrongPlus.text.toString().toInt() + 1).toString()
            }
        }
        btnPlus2.setOnClickListener {
            if (btnPlus2.text.toString().toInt() == sum) {
                resultPlus.setText("Correct")
                correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

            } else {
                resultPlus.setText("Wrong")
                wrongPlus.text = (wrongPlus.text.toString().toInt() + 1).toString()
            }
        }
        btnPlus3.setOnClickListener {
            if (btnPlus3.text.toString().toInt() == sum) {
                resultPlus.setText("Correct")
                correctPlus.text = (correctPlus.text.toString().toInt() + 1).toString()

            } else {
                resultPlus.setText("Wrong")
                wrongPlus.text = (wrongPlus.text.toString().toInt() + 1).toString()
            }
        }
    }


}